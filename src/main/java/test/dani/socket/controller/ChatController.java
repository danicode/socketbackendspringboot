package test.dani.socket.controller;

import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import test.dani.socket.dto.Mensaje;
import test.dani.socket.service.MensajeService;

@Controller
public class ChatController {
	@Autowired
	MensajeService mensajeService;
	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;
	
	private String[] colores = {"red","green", "blue", "magenta", "purple", "orange"};
	@MessageMapping("/mensaje")
	@SendTo("/chat/mensaje")
	public Mensaje recibeMensaje(Mensaje mensaje) {
		mensaje.setFecha(new Date());
		if (mensaje.getTipo().equals("NUEVO_USUARIO")) {
			mensaje.setTexto("Nuevo Usuario");
			mensaje.setColor(colores[new Random().nextInt(colores.length)]);
		} else {
			mensajeService.RegistrarMensaje(mensaje);
		}
		return mensaje;
	}
	
	@MessageMapping("/escribiendo")
	@SendTo("/chat/escribiendo")
	public String estaEscribiendo(String username) {
		System.out.println(":v");
		return username + " está escribiendo...";
	}
	
	@MessageMapping("/historial")
	public void historial(String clienteId) {
		System.out.println(clienteId);
		simpMessagingTemplate.convertAndSend("/chat/historial/"+clienteId,mensajeService.listarMensajes(10));
	}

}
