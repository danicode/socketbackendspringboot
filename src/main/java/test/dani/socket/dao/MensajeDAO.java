package test.dani.socket.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import test.dani.socket.dto.Mensaje;

@Repository
public interface MensajeDAO {
	List<Mensaje> listarMensajes(Integer tope);
	Integer registrarMensaje(Mensaje mensaje);
}
