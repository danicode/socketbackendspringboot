package test.dani.socket.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.dani.socket.dao.MensajeDAO;
import test.dani.socket.dto.Mensaje;
@Service
public class MensajeServiceImpl implements MensajeService {
	@Autowired
	MensajeDAO mensajeDAO;
	@Override
	public List<Mensaje> listarMensajes(Integer tope) {
		return mensajeDAO.listarMensajes(tope);
	}
	@Override
	public Integer RegistrarMensaje(Mensaje mensaje) {
		Integer algo = mensajeDAO.registrarMensaje(mensaje); 
		System.out.println(algo);
		return algo;
	}
}