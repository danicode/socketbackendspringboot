package test.dani.socket.service;

import java.util.List;

import test.dani.socket.dto.Mensaje;

public interface MensajeService {
	List <Mensaje> listarMensajes(Integer tope);
	Integer RegistrarMensaje(Mensaje mensaje);
}